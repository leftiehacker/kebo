# About:
- Go to the [Heroku project](https://kebo.herokuapp.com/)
- Adjust your volume all the way down then back up to find the right level
- Play music with the A, B, C, D, E, F, and G keys on your keyboard
- Hold `shift` for sharps, `alt` or `option` for flats
- Change the octave by tapping a number between 0 and 7
